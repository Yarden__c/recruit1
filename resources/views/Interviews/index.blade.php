@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of Interviews</h1>

<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Summery</th><th>Candidate</th><th>User</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @forelse($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->summery}}</td>
            <td>@if(isset($interview->candidate_id))
                {{$interview->candidate->name}}
                @endif </td>
            <td>@if(isset($interview->user_id))
                {{$interview->user->name}}
                @endif </td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
    @empty
    <p><div class = 'alert alert-danger'>There is no interviews for this user</div></p>        
    @endforelse
</table>

@endsection

