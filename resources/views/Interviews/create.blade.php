@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
<div class = "text-center">
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group row">
            <label for = "name" class="col-md-4 col-form-label text-md-right">Interview date</label>
            <div class="col-md-6">
            <input type = "date" class="form-control" name = "date">
            </div> 
        </div>     
        <div class="form-group row">
            <label for = "email" class="col-md-4 col-form-label text-md-right">Interview summery</label>
            <div class="col-md-6">
            <input type = "text" class="form-control" name = "summery">
        </div> 
        </div>
        <div class="form-group row">
                            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">Candidate</label>
                            <div class="col-md-6">
                                <select class="form-control" name="candidate_id">                                                                         
                                   @foreach ($candidates as $candidate)
                                     <option value="{{ $candidate->id }}"> 
                                         {{ $candidate->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>
                            <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">User</label>
                            <div class="col-md-6">
                                <select class="form-control" name="user_id">                                                                      
                                   @foreach ($users as $user)
                                     <option value="{{ $user->id }}" > 
                                         {{ $user->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
        </div>
@endsection
