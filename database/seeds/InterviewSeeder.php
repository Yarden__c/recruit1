<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '15.07.20',
                'summery' => 'interview in laravel framework',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '16.07.20',
                'summery' => 'interview in python',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]); 
    }
}
